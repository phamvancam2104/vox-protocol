/*

website: vox.finance

 _    ______ _  __   ___________   _____    _   ______________
| |  / / __ \ |/ /  / ____/  _/ | / /   |  / | / / ____/ ____/
| | / / / / /   /  / /_   / //  |/ / /| | /  |/ / /   / __/   
| |/ / /_/ /   |_ / __/ _/ // /|  / ___ |/ /|  / /___/ /___   
|___/\____/_/|_(_)_/   /___/_/ |_/_/  |_/_/ |_/\____/_____/   
                                                              
*/

pragma solidity 0.6.12;

import "./token/ERC20.sol";
import "./access/Ownable.sol";
import "./access/Mintable.sol";

// VoxToken
contract VoxToken is ERC20("Vox.Finance", "VOX", 18, 0, 1250), Ownable, Mintable {
    /// @notice Creates `_amount` token to `_to`. Must only be called by the minter (VoxMaster).
    function mint(address _to, uint256 _amount) public onlyMinter {
        _mint(_to, _amount);
    }

    function setBurnrate(uint8 burnrate_) public onlyOwner {
        _setupBurnrate(burnrate_);
    }

    function addWhitelistedAddress(address _address) public onlyOwner {
        _whitelistedAddresses[_address] = true;
    }

    function removeWhitelistedAddress(address _address) public onlyOwner {
        _whitelistedAddresses[_address] = false;
    }
}