/*

website: vox.finance

 _    ______ _  __   ___________   _____    _   ______________
| |  / / __ \ |/ /  / ____/  _/ | / /   |  / | / / ____/ ____/
| | / / / / /   /  / /_   / //  |/ / /| | /  |/ / /   / __/   
| |/ / /_/ /   |_ / __/ _/ // /|  / ___ |/ /|  / /___/ /___   
|___/\____/_/|_(_)_/   /___/_/ |_/_/  |_/_/ |_/\____/_____/   
                                                              
*/

pragma solidity 0.6.12;

import "./token/IERC20.sol";
import "./token/SafeERC20.sol";
import "./utils/EnumerableSet.sol";
import "./math/SafeMath.sol";
import "./access/Ownable.sol";

import "./VoxToken.sol";
import "./VoxPopuliToken.sol";

interface IMigrationMaster {
    // Perform LP token migration for swapping liquidity providers.
    // Take the current LP token address and return the new LP token address.
    // Migrator should have full access to the caller's LP token.
    // Return the new LP token address.
    //
    // Migrator must have allowance access to old LP tokens.
    // EXACTLY the same amount of new LP tokens must be minted or
    // else something bad will happen. This function is meant to be used to swap
    // to the native liquidity provider in the future (see the VOX roadmap)!
    function migrate(IERC20 token) external returns (IERC20);
}

contract VoxMaster is Ownable {
    using SafeMath for uint256;
    using SafeERC20 for IERC20;

    // Info of each user.
    struct UserInfo {
        uint256 amount; // How many LP tokens the user has provided.
        uint256 rewardDebt; // Reward debt. See explanation below.
        uint256 pendingRewards; // Pending rewards for user.
        //
        // We do some fancy math here. Basically, any point in time, the amount of VOXs
        // entitled to a user but is pending to be distributed is:
        //
        //   pending reward = (user.amount * pool.accVoxPerShare) - user.rewardDebt
        //
        // Whenever a user deposits or withdraws LP tokens to a pool. Here's what happens:
        //   1. The pool's `accVoxPerShare` (and `lastRewardBlock`) gets updated.
        //   2. User receives the pending reward sent to his/her address.
        //   3. User's `amount` gets updated.
        //   4. User's `rewardDebt` gets updated.
    }

    // Info of each pool.
    struct PoolInfo {
        IERC20 lpToken; // Address of LP token contract.
        uint256 allocPoint; // How many allocation points assigned to this pool. VOXs to distribute per block.
        uint256 lastRewardBlock; // Last block number that VOXs distribution occurs.
        uint256 accVoxPerShare; // Accumulated VOXs per share, times 1e12. See below.
    }

    // VOX token
    VoxToken public vox;
    // POPULI token
    VoxPopuliToken public populi;
    // Dev fund (4%, initially)
    uint256 public devFundDivRate = 25;
    // Dev address.
    address public devaddr;
    // Block number when bonus VOX period ends.
    uint256 public bonusEndBlock;
    // VOX tokens created per block.
    uint256 public voxPerBlock;
    // Bonus muliplier for early vox liquidity providers.
    uint256 public BONUS_MULTIPLIER = 1;
    // The migrator contract. It has a lot of power. Can only be set through governance (owner).
    IMigrationMaster public migrator;

    // Info of each pool.
    PoolInfo[] public poolInfo;
    // Info of each user that stakes LP tokens.
    mapping(uint256 => mapping(address => UserInfo)) public userInfo;
    // Total allocation points. Must be the sum of all allocation points in all pools.
    uint256 public totalAllocPoint = 0;
    // The block number when VOX mining starts.
    uint256 public startBlock;

    // Events
    event Recovered(address token, uint256 amount);
    event Deposit(address indexed user, uint256 indexed pid, uint256 amount);
    event Withdraw(address indexed user, uint256 indexed pid, uint256 amount);
    event Claim(address indexed user, uint256 indexed pid, uint256 amount);
    event ClaimAndStake(address indexed user, uint256 indexed pid, uint256 amount);
    event EmergencyWithdraw(
        address indexed user,
        uint256 indexed pid,
        uint256 amount
    );

    constructor(
        VoxToken _vox,
        VoxPopuliToken _populi,
        address _devaddr,
        uint256 _voxPerBlock,
        uint256 _startBlock,
        uint256 _bonusEndBlock
    ) public {
        vox = _vox;
        populi = _populi;
        devaddr = _devaddr;
        voxPerBlock = _voxPerBlock;
        bonusEndBlock = _bonusEndBlock;
        startBlock = _startBlock;

        // staking pool
        poolInfo.push(PoolInfo({
            lpToken: _vox,
            allocPoint: 1000,
            lastRewardBlock: startBlock,
            accVoxPerShare: 0
        }));

        totalAllocPoint = 1000;
    }

    function poolLength() external view returns (uint256) {
        return poolInfo.length;
    }

    // Return reward multiplier over the given _from to _to block.
    function getMultiplier(uint256 _from, uint256 _to)
        public
        view
        returns (uint256)
    {
        if (_to <= bonusEndBlock) {
            return _to.sub(_from).mul(BONUS_MULTIPLIER);
        } else if (_from >= bonusEndBlock) {
            return _to.sub(_from);
        } else {
            return
                bonusEndBlock.sub(_from).mul(BONUS_MULTIPLIER).add(
                    _to.sub(bonusEndBlock)
                );
        }
    }

    // Add a new lp to the pool. Can only be called by the owner.
    // XXX DO NOT add the same LP token more than once. Rewards will be messed up if you do.
    function add(
        uint256 _allocPoint,
        IERC20 _lpToken,
        bool _withUpdate
    ) public onlyOwner {
        if (_withUpdate) {
            massUpdatePools();
        }
        uint256 lastRewardBlock = block.number > startBlock
            ? block.number
            : startBlock;
        totalAllocPoint = totalAllocPoint.add(_allocPoint);
        poolInfo.push(
            PoolInfo({
                lpToken: _lpToken,
                allocPoint: _allocPoint,
                lastRewardBlock: lastRewardBlock,
                accVoxPerShare: 0
            })
        );
        updateStakingPool();
    }

    // Update the given pool's VOX allocation point. Can only be called by the owner.
    function set(
        uint256 _pid,
        uint256 _allocPoint,
        bool _withUpdate
    ) public onlyOwner {
        if (_withUpdate) {
            massUpdatePools();
        }
        totalAllocPoint = totalAllocPoint.sub(poolInfo[_pid].allocPoint).add(
            _allocPoint
        );
        uint256 prevAllocPoint = poolInfo[_pid].allocPoint;
        poolInfo[_pid].allocPoint = _allocPoint;
        if (prevAllocPoint != _allocPoint) {
            updateStakingPool();
        }
    }

    function updateStakingPool() internal {
        uint256 length = poolInfo.length;
        uint256 points = 0;
        for (uint256 pid = 1; pid < length; ++pid) {
            points = points.add(poolInfo[pid].allocPoint);
        }
        if (points != 0) {
            points = points.div(3);
            totalAllocPoint = totalAllocPoint.sub(poolInfo[0].allocPoint).add(points);
            poolInfo[0].allocPoint = points;
        }
    }

    // Migrate lp token to another lp contract. Can be called by anyone. 
    // We trust that migrator contract is good.
    function migrate(uint256 _pid) public {
        require(address(migrator) != address(0), "migrate: no migrator");
        PoolInfo storage pool = poolInfo[_pid];
        IERC20 lpToken = pool.lpToken;
        uint256 bal = lpToken.balanceOf(address(this));
        lpToken.safeApprove(address(migrator), bal);
        IERC20 newLpToken = migrator.migrate(lpToken);
        require(bal == newLpToken.balanceOf(address(this)), "migrate: bad");
        pool.lpToken = newLpToken;
    }

    // View function to see pending VOXs on frontend.
    function pendingVox(uint256 _pid, address _user)
        external
        view
        returns (uint256)
    {
        PoolInfo storage pool = poolInfo[_pid];
        UserInfo storage user = userInfo[_pid][_user];
        uint256 accVoxPerShare = pool.accVoxPerShare;
        uint256 lpSupply = pool.lpToken.balanceOf(address(this));
        if (block.number > pool.lastRewardBlock && lpSupply != 0) {
            uint256 multiplier = getMultiplier(
                pool.lastRewardBlock,
                block.number
            );
            uint256 voxReward = multiplier
                .mul(voxPerBlock)
                .mul(pool.allocPoint)
                .div(totalAllocPoint);
            accVoxPerShare = accVoxPerShare.add(
                voxReward.mul(1e12).div(lpSupply)
            );
        }
        return
            user.amount.mul(accVoxPerShare).div(1e12).sub(user.rewardDebt).add(user.pendingRewards);
    }

    // Update reward vairables for all pools. Be careful of gas spending!
    function massUpdatePools() public {
        uint256 length = poolInfo.length;
        for (uint256 pid = 0; pid < length; ++pid) {
            updatePool(pid);
        }
    }

    // Update reward variables of the given pool to be up-to-date.
    function updatePool(uint256 _pid) public {
        PoolInfo storage pool = poolInfo[_pid];
        if (block.number <= pool.lastRewardBlock) {
            return;
        }
        uint256 lpSupply = pool.lpToken.balanceOf(address(this));
        if (lpSupply == 0) {
            pool.lastRewardBlock = block.number;
            return;
        }
        uint256 multiplier = getMultiplier(pool.lastRewardBlock, block.number);
        uint256 voxReward = multiplier
            .mul(voxPerBlock)
            .mul(pool.allocPoint)
            .div(totalAllocPoint);
        vox.mint(devaddr, voxReward.div(devFundDivRate));
        vox.mint(address(this), voxReward);
        pool.accVoxPerShare = pool.accVoxPerShare.add(
            voxReward.mul(1e12).div(lpSupply)
        );
        pool.lastRewardBlock = block.number;
    }

    // Deposit LP tokens to VoxMaster for VOX allocation.
    function deposit(uint256 _pid, uint256 _amount, bool _withdrawRewards) public {
        require (_pid != 0, 'please deposit VOX by staking');
        PoolInfo storage pool = poolInfo[_pid];
        UserInfo storage user = userInfo[_pid][msg.sender];
        updatePool(_pid);
        if (user.amount > 0) {
            uint256 pending = user
                .amount
                .mul(pool.accVoxPerShare)
                .div(1e12)
                .sub(user.rewardDebt);
            
            if (pending > 0) {
                user.pendingRewards = user.pendingRewards.add(pending);

                if (_withdrawRewards) {
                    safeVoxTransfer(msg.sender, user.pendingRewards);
                    emit Claim(msg.sender, _pid, user.pendingRewards);
                    user.pendingRewards = 0;
                }
            }
        }
        if (_amount > 0) {
            pool.lpToken.safeTransferFrom(address(msg.sender), address(this), _amount);
            user.amount = user.amount.add(_amount);
        }
        user.rewardDebt = user.amount.mul(pool.accVoxPerShare).div(1e12);
        emit Deposit(msg.sender, _pid, _amount);
    }

    // Withdraw LP tokens from VoxMaster.
    function withdraw(uint256 _pid, uint256 _amount, bool _withdrawRewards) public {
        require (_pid != 0, 'please withdraw VOX by unstaking');
        PoolInfo storage pool = poolInfo[_pid];
        UserInfo storage user = userInfo[_pid][msg.sender];
        require(user.amount >= _amount, "withdraw: not good");
        updatePool(_pid);
        uint256 pending = user.amount.mul(pool.accVoxPerShare).div(1e12).sub(user.rewardDebt);
        if (pending > 0) {
            user.pendingRewards = user.pendingRewards.add(pending);

            if (_withdrawRewards) {
                safeVoxTransfer(msg.sender, user.pendingRewards);
                emit Claim(msg.sender, _pid, user.pendingRewards);
                user.pendingRewards = 0;
            }
        }
        if (_amount > 0) {
            user.amount = user.amount.sub(_amount);
            pool.lpToken.safeTransfer(address(msg.sender), _amount);
        }
        user.rewardDebt = user.amount.mul(pool.accVoxPerShare).div(1e12);
        emit Withdraw(msg.sender, _pid, _amount);
    }

    // Withdraw without caring about rewards. EMERGENCY ONLY.
    function emergencyWithdraw(uint256 _pid) public {
        require (_pid != 0, 'please withdraw VOX by unstaking');
        PoolInfo storage pool = poolInfo[_pid];
        UserInfo storage user = userInfo[_pid][msg.sender];
        pool.lpToken.safeTransfer(address(msg.sender), user.amount);
        emit EmergencyWithdraw(msg.sender, _pid, user.amount);
        user.amount = 0;
        user.rewardDebt = 0;
        user.pendingRewards = 0;
    }

    // Claim rewards from VoxMaster.
    function claim(uint256 _pid) public {
        require (_pid != 0, 'please claim staking rewards on stake page');
        PoolInfo storage pool = poolInfo[_pid];
        UserInfo storage user = userInfo[_pid][msg.sender];
        updatePool(_pid);
        uint256 pending = user.amount.mul(pool.accVoxPerShare).div(1e12).sub(user.rewardDebt);
        if (pending > 0 || user.pendingRewards > 0) {
            user.pendingRewards = user.pendingRewards.add(pending);
            safeVoxTransfer(msg.sender, user.pendingRewards);
            emit Claim(msg.sender, _pid, user.pendingRewards);
            user.pendingRewards = 0;
        }
        user.rewardDebt = user.amount.mul(pool.accVoxPerShare).div(1e12);
    }

    // Claim rewards from VoxMaster and deposit them directly to staking pool.
    function claimAndStake(uint256 _pid) public {
        require (_pid != 0, 'please claim and stake staking rewards on stake page');
        PoolInfo storage pool = poolInfo[_pid];
        UserInfo storage user = userInfo[_pid][msg.sender];
        updatePool(_pid);
        uint256 pending = user.amount.mul(pool.accVoxPerShare).div(1e12).sub(user.rewardDebt);
        if (pending > 0 || user.pendingRewards > 0) {
            user.pendingRewards = user.pendingRewards.add(pending);
            transferToStake(user.pendingRewards);
            emit ClaimAndStake(msg.sender, _pid, user.pendingRewards);
            user.pendingRewards = 0;
        }
        user.rewardDebt = user.amount.mul(pool.accVoxPerShare).div(1e12);
    }

    // Transfer rewards from LP pools to staking pool.
    function transferToStake(uint256 _amount) internal {
        PoolInfo storage pool = poolInfo[0];
        UserInfo storage user = userInfo[0][msg.sender];
        updatePool(0);
        if (user.amount > 0) {
            uint256 pending = user.amount.mul(pool.accVoxPerShare).div(1e12).sub(user.rewardDebt);
            if (pending > 0) {
                user.pendingRewards = user.pendingRewards.add(pending);
            }
        }
        if (_amount > 0) {
            user.amount = user.amount.add(_amount);
        }
        user.rewardDebt = user.amount.mul(pool.accVoxPerShare).div(1e12);

        populi.mint(msg.sender, _amount);
        emit Deposit(msg.sender, 0, _amount);
    }

    // Stake VOX tokens to VoxMaster.
    function enterStaking(uint256 _amount, bool _withdrawRewards) public {
        PoolInfo storage pool = poolInfo[0];
        UserInfo storage user = userInfo[0][msg.sender];
        updatePool(0);
        if (user.amount > 0) {
            uint256 pending = user.amount.mul(pool.accVoxPerShare).div(1e12).sub(user.rewardDebt);
            if (pending > 0) {
                user.pendingRewards = user.pendingRewards.add(pending);

                if (_withdrawRewards) {
                    safeVoxTransfer(msg.sender, user.pendingRewards);
                    user.pendingRewards = 0;
                }
            }
        }
        if (_amount > 0) {
            pool.lpToken.safeTransferFrom(address(msg.sender), address(this), _amount);
            user.amount = user.amount.add(_amount);
        }
        user.rewardDebt = user.amount.mul(pool.accVoxPerShare).div(1e12);

        populi.mint(msg.sender, _amount);
        emit Deposit(msg.sender, 0, _amount);
    }

    // Withdraw VOX tokens from staking.
    function leaveStaking(uint256 _amount, bool _withdrawRewards) public {
        PoolInfo storage pool = poolInfo[0];
        UserInfo storage user = userInfo[0][msg.sender];
        require(user.amount >= _amount, "unstake: not good");
        require(populi.balanceOf(msg.sender) >= _amount, "unstake: not enough POPULI tokens");
        updatePool(0);
        uint256 pending = user.amount.mul(pool.accVoxPerShare).div(1e12).sub(user.rewardDebt);
        if (pending > 0) {
                user.pendingRewards = user.pendingRewards.add(pending);

                if (_withdrawRewards) {
                    safeVoxTransfer(msg.sender, user.pendingRewards);
                    user.pendingRewards = 0;
                }
            }
        if (_amount > 0) {
            user.amount = user.amount.sub(_amount);
            pool.lpToken.safeTransfer(address(msg.sender), _amount);
        }
        user.rewardDebt = user.amount.mul(pool.accVoxPerShare).div(1e12);

        populi.burn(msg.sender, _amount);
        emit Withdraw(msg.sender, 0, _amount);
    }

    // Claim staking rewards from VoxMaster.
    function claimStaking() public {
        PoolInfo storage pool = poolInfo[0];
        UserInfo storage user = userInfo[0][msg.sender];
        updatePool(0);
        uint256 pending = user.amount.mul(pool.accVoxPerShare).div(1e12).sub(user.rewardDebt);
        if (pending > 0 || user.pendingRewards > 0) {
            user.pendingRewards = user.pendingRewards.add(pending);
            safeVoxTransfer(msg.sender, user.pendingRewards);
            emit Claim(msg.sender, 0, user.pendingRewards);
            user.pendingRewards = 0;
        }
        user.rewardDebt = user.amount.mul(pool.accVoxPerShare).div(1e12);
    }

    // Transfer staking rewards to staking pool.
    function stakeRewardsStaking() public {
        PoolInfo storage pool = poolInfo[0];
        UserInfo storage user = userInfo[0][msg.sender];
        uint256 rewardsToStake;
        updatePool(0);
        if (user.amount > 0) {
            uint256 pending = user.amount.mul(pool.accVoxPerShare).div(1e12).sub(user.rewardDebt);
            if (pending > 0) {
                user.pendingRewards = user.pendingRewards.add(pending);
            }
        }
        if (user.pendingRewards > 0) {
            rewardsToStake = user.pendingRewards;
            user.pendingRewards = 0;
            user.amount = user.amount.add(rewardsToStake);
        }
        user.rewardDebt = user.amount.mul(pool.accVoxPerShare).div(1e12);

        populi.mint(msg.sender, rewardsToStake);
        emit Deposit(msg.sender, 0, rewardsToStake);
    }

    // Safe vox transfer function, just in case if rounding error causes pool to not have enough VOXs.
    function safeVoxTransfer(address _to, uint256 _amount) internal {
        uint256 voxBal = vox.balanceOf(address(this));
        if (_amount > voxBal) {
            vox.transfer(_to, voxBal);
        } else {
            vox.transfer(_to, _amount);
        }
    }

    // Update dev address by the previous dev.
    function setDevAddr(address _devaddr) public {
        require(msg.sender == devaddr, "!dev: nice try, amigo");
        devaddr = _devaddr;
    }

    // **** Additional functions to edit master attributes ****

    function setVoxPerBlock(uint256 _voxPerBlock) public onlyOwner {
        require(_voxPerBlock > 0, "!voxPerBlock-0");
        voxPerBlock = _voxPerBlock;
    }

    function setBonusMultiplier(uint256 _bonusMultiplier) public onlyOwner {
        require(_bonusMultiplier > 0, "!bonusMultiplier-0");
        BONUS_MULTIPLIER = _bonusMultiplier;
    }

    function setBonusEndBlock(uint256 _bonusEndBlock) public onlyOwner {
        bonusEndBlock = _bonusEndBlock;
    }

    // Set the migrator contract. Can only be called by the owner.
    function setMigrator(IMigrationMaster _migrator) public onlyOwner {
        migrator = _migrator;
    }

    function setDevFundDivRate(uint256 _devFundDivRate) public onlyOwner {
        require(_devFundDivRate > 0, "!devFundDivRate-0");
        devFundDivRate = _devFundDivRate;
    }
}
