pragma solidity 0.6.12;

interface ICurveTBTC {
    function add_liquidity(
        uint256[4] calldata amounts,
        uint256 min_mint_amount
    ) external returns (uint256);
}