pragma solidity 0.6.12;

interface IKeepRewardsClaimable {
  function claim_rewards() external;
}