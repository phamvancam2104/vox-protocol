pragma solidity 0.6.12;

interface IStrategy {
    function rewards() external view returns (address);

    function gauge() external view returns (address);

    function underlying() external view returns (address);

    function timelock() external view returns (address);

    function deposit() external;

    function withdraw(uint256) external;

    function withdrawAll() external returns (uint256);

    function balanceOf() external view returns (uint256);

    function harvest() external;

    function salvage(address) external;

    function setTimelock(address _timelock) external;

    function setGovernance(address _governance) external;

    function setTreasury(address _treasury) external;
}